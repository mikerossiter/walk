# 🥾 walk

### Forked changes
Amended the navigation keys to WASD for nerdy gamers like me as well as the enter and exit keys to left and right arrows instead. "xx" will delete an item. 

Walk — a terminal navigator.

Why another terminal navigator? I wanted something simple and minimalistic.
Something to help me with faster navigation in the filesystem; a `cd` and `ls`
replacement. So I build "walk". It allows for quick navigation with fuzzy
searching. `cd` integration is quite simple. And you can open `vim` right from
the walk. That's it.

## Install

```
cd walk
go build
cp walk /usr/local/bin

$ walk --icons
```
